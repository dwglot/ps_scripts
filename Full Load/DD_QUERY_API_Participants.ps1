﻿#####################################################################################################################
###
### Powershell Script to retrieve DonorDrive Events data
### Ver 1.0
### Chaitanya Ramagiri - 08/27/2020
###
### Connects to DonorDrive host and retrieves the DonorDrive Events data for TABS Analytics feed.
###
#####################################################################################################################


$apiUrl = "https://api.donordrive.com/v1/mssociety/view/participants.json?select=recordid,participantconstituentid,participantlastname,participantfirstname,participantemail,participantrole,participantregistereddate,fundraisingpageisactive,participantfundraisinggoal,isreturningparticipant,participantregistrationfee,participantsumdonationsmade,participantteamcaptainid,teamid,modifieddate,participantnumdonorsolicitations"
$uid = "P2PIntegration@nmss.org"

$pwd =  Get-Content "F:\Donor_Drive\Misc\UC\pc.txt"

#$credPair = "$($uid):$($pwd)"
#$encodedCredentials = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($credPair))

$headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$headers.Add("donordrive-email", $uid)
$headers.Add("donordrive-password", $pwd)
$headers.Add("Cookie", "__cfduid=da09efa55545103057a141f76b12568331597068523; __cfruid=35ffd0c8af1cde3bb3eca4e4726db35358e6fb1a-1598477975; AWSALB=FwxCde8cSjB3aS2rV6MttYkBFxriJuvp/NkJJ7x+TLDBb0gyXxfWIpy45iogHL4Uza0Wc7nuZ06VC+9y7MNBlmFkN85T3nXjt1CJbuob0Zp3v1w2L/MOam27AUi1; AWSALBCORS=FwxCde8cSjB3aS2rV6MttYkBFxriJuvp/NkJJ7x+TLDBb0gyXxfWIpy45iogHL4Uza0Wc7nuZ06VC+9y7MNBlmFkN85T3nXjt1CJbuob0Zp3v1w2L/MOam27AUi1")

[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"

try
{
$response = Invoke-WebRequest -Uri $apiUrl  -Method 'GET' -Headers $headers  
$response | ConvertFrom-Json|select-object -ExpandProperty Result | Export-Csv -Path "F:\Donor_Drive\Inbound_Files\Participants.csv" -NoTypeInformation -force
}
catch

{
$Request = $_.Exception
    Write-host "Exception caught: $Request"    
}


#####################################################################################################################



